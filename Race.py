import pygame as pg
import Race_objects
from pygame.math import Vector2

size = [1200, 700]

BUTTON_Y = 2
BUTTON_B = 0
BUTTON_A = 1


class RaceGame():
    def __init__(self):
        pg.init()
        pg.joystick.init()
        self.joystick = pg.joystick.Joystick(0)
        self.joystick2 = pg.joystick.Joystick(1)
        self.joystick.init()
        self.joystick2.init()
        self.clock = pg.time.Clock()
        self.fps = 90
        self.done = False
        self.screen = pg.display.set_mode(size)
        self.player1 = Race_objects.Player(Vector2(450,538), 'racecar.png')
        self.player2 = Race_objects.Player(Vector2(370,596), 'racecar blue.png')
        self.player3 = Race_objects.Player(Vector2(450,596), 'racecar green.png')
        self.player4 = Race_objects.Player(Vector2(370,538), 'racecar orange.png')
        self.all_sprites = pg.sprite.Group()

        self.border_right = Race_objects.Border(Vector2(1180,0),20,700, (0,0,0))
        self.all_sprites.add(self.border_right)
        self.border_left = Race_objects.Border(Vector2(0,0),20,700, (0,0,0))
        self.all_sprites.add(self.border_left)
        self.border_top = Race_objects.Border(Vector2(0,0),1200,20, (0,0,0))
        self.all_sprites.add(self.border_top)
        self.border_bot = Race_objects.Border(Vector2(0,680),1200,20, (0,0,0))
        self.all_sprites.add(self.border_bot)
        self.border_mid = Race_objects.Border(Vector2(300,200),600,300, (0,0,0))
        self.all_sprites.add(self.border_mid)

        self.all_borders = pg.sprite.Group()

        self.all_borders.add(self.border_right)
        self.all_borders.add(self.border_left)
        self.all_borders.add(self.border_top)
        self.all_borders.add(self.border_bot)
        self.all_borders.add(self.border_mid)

        self.checkpoint_1 = Race_objects.Border(Vector2(900,500),280,180, (255,255,255))
        self.checkpoint_2 = Race_objects.Border(Vector2(900,20),280,180, (255,255,255))
        self.checkpoint_3 = Race_objects.Border(Vector2(20,20),280,180, (255,255,255))
        self.checkpoint_4 = Race_objects.Border(Vector2(20,500),280,180, (255,255,255))
        self.finish_line = Race_objects.FinishLine(Vector2(556,500))

        self.checkpoints = pg.sprite.Group()
        self.checkpoints.add(self.checkpoint_1)
        self.checkpoints.add(self.checkpoint_2)
        self.checkpoints.add(self.checkpoint_3)
        self.checkpoints.add(self.checkpoint_4)
        self.checkpoints.add(self.finish_line)

        self.all_sprites.add(self.checkpoint_1)
        self.all_sprites.add(self.checkpoint_2)
        self.all_sprites.add(self.checkpoint_3)
        self.all_sprites.add(self.checkpoint_4)
        self.all_sprites.add(self.finish_line)

        self.all_sprites.add(self.player1)
        self.all_sprites.add(self.player2)
        self.all_sprites.add(self.player3)
        self.all_sprites.add(self.player4)

        self.checkpoint_counter_1 = 0
        self.checkpoint_counter_2 = 0
        self.checkpoint_counter_3 = 0
        self.checkpoint_counter_4 = 0

        self.course_counter_1 = 0
        self.course_counter_2 = 0
        self.course_counter_3 = 0
        self.course_counter_4 = 0

        self.font_course = pg.font.SysFont("comicsansms", 20)

    def round_check(self):
        if pg.sprite.collide_rect(self.player1, self.checkpoint_1):
            if self.checkpoint_counter_1 == 0:
                self.checkpoint_counter_1 += 1

        if pg.sprite.collide_rect(self.player1, self.checkpoint_2):
            if self.checkpoint_counter_1 == 1:
                self.checkpoint_counter_1 += 1
        if pg.sprite.collide_rect(self.player1, self.checkpoint_3):
            if self.checkpoint_counter_1 == 2:
                self.checkpoint_counter_1 += 1
        if pg.sprite.collide_rect(self.player1, self.checkpoint_4):
            if self.checkpoint_counter_1 == 3:
                self.checkpoint_counter_1 += 1
        if pg.sprite.collide_rect(self.player1, self.finish_line):
            if self.checkpoint_counter_1 == 4:
                self.checkpoint_counter_1 = 0
                self.course_counter_1 += 1

        if pg.sprite.collide_rect(self.player2, self.checkpoint_1):
            if self.checkpoint_counter_2 == 0:
                self.checkpoint_counter_2 += 1

        if pg.sprite.collide_rect(self.player2, self.checkpoint_2):
            if self.checkpoint_counter_2 == 1:
                self.checkpoint_counter_2 += 1
        if pg.sprite.collide_rect(self.player2, self.checkpoint_3):
            if self.checkpoint_counter_2 == 2:
                self.checkpoint_counter_2 += 1
        if pg.sprite.collide_rect(self.player2, self.checkpoint_4):
            if self.checkpoint_counter_2 == 3:
                self.checkpoint_counter_2 += 1
        if pg.sprite.collide_rect(self.player2, self.finish_line):
            if self.checkpoint_counter_2 == 4:
                self.checkpoint_counter_2 = 0
                self.course_counter_2 += 1

    def event_loop(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.done = True

        x = self.joystick.get_axis(0)
        y = self.joystick.get_axis(1)
        x2 = self.joystick2.get_axis(0)
        y2 = self.joystick2.get_axis(1)
        if self.joystick.get_button(BUTTON_Y):
            self.player1.accelerate()
        if self.joystick2.get_button(BUTTON_Y):
            self.player2.accelerate()
        if self.joystick.get_button(BUTTON_B):
            self.player1.brake()
        if self.joystick2.get_button(BUTTON_B):
            self.player2.brake()
        if self.joystick.get_axis(0):
            self.player1.turn(-x)
        if self.joystick2.get_axis(0):
            self.player2.turn(-x2)


    def update(self):
        for sprite in self.all_sprites:
            sprite.update()
        if pg.sprite.spritecollide(self.player1, self.all_borders, False):
            self.player1.velocity = 0
        if pg.sprite.spritecollide(self.player2, self.all_borders, False):
            self.player2.velocity = 0
        self.round_check()
        if pg.sprite.collide_rect(self.player1, self.player2):
            self.player2.velocity = -5
            self.player1.velocity = -5
        print (self.checkpoint_counter_1)

        self.text1 = self.font_course.render(str(self.course_counter_1), False, (200, 0, 0))
        self.text2 = self.font_course.render(str(self.course_counter_2), False, (0, 0, 200))
    def draw(self):
        self.screen.fill((255,255,255))
        self.all_sprites.draw(self.screen)
        self.screen.blit(self.text1, (600, 350))
        self.screen.blit(self.text2, (650, 350))
    def run(self):
        while not self.done:
            self.event_loop()
            self.update()
            self.draw()
            pg.display.update()
            self.clock.tick(self.fps)

game = RaceGame()
game.run()
