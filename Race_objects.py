import pygame as pg
from pygame.math import Vector2
import math

VELOCITY_SCALE = 20
RESISTANCE_SCALE = 50
class Player(pg.sprite.Sprite):
    def __init__(self, position, image):
        pg.sprite.Sprite.__init__(self)
        self.position = position

        image = pg.image.load(image)
        #self.image = pg.transform.scale(self.image, (53,42))
        image = pg.transform.scale(image, (53,42))
        self.image=pg.Surface((51,40), flags=pg.SRCALPHA)
        self.image.blit(image, (0,0), area=pg.Rect(2,0,53,42))
        #self.offset = 0.5*Vector2(53,42)

        self.image_orig = self.image
        self.rect = self.image.get_rect(center=(self.position.x, self.position.y))
        #self.rect.x = self.position.x
        #self.rect.y = self.position.y
        self.velocity = 0
        self.direction = 0
        self.max_speed = 10/2
        self.max_brake_speed = -5/2
        self.resistance_value = 1 / RESISTANCE_SCALE

    def accelerate(self):
        self.velocity += 1 / VELOCITY_SCALE

    def brake(self):
        self.velocity -= 1 / VELOCITY_SCALE

    def turn(self, angle):
        self.direction += angle
        #self.image = pg.transform.rotozoom(self.image_orig, self.direction, 1)
        self.image=pg.transform.rotate(self.image_orig, self.direction)
        #offset_rotated = self.offset.rotate(-self.direction)
        #(x, y) = self.position + offset_rotated
        self.rect = self.image.get_rect(center=self.position)
        #orig_rect = self.image.get_rect()
        #new_image = pg.transform.rotate(self.image_orig, self.direction)
        #rot_rect = orig_re1t.copy()
        #rot_rect.center = new_image.get_rect.center()
        #self.image = new_image.subsurface(rot_rect).copy()
        #self.rect = self.image.get_rect()
        #self.rect = self.image.get_rect()
        #d = self.direction / 360 * math.pi
        #self.rect.center = [old_center[0]-hSize[0] * (1/math.sin(d)),old_center[1]-hSize[1]]
        #self.rect.center = [0,0]
        #print(self.direction)

    def update(self):
        v = Vector2(0,0)
        v.from_polar((self.velocity, -self.direction))
        self.position += v
        #self.rect.centerx = self.position.x - self.rect.width/2
        #self.rect.centery = self.position.y - self.rect.height/2
        if self.velocity > 0:
            self.velocity -= self.resistance_value
        elif self.velocity < 0:
            self.velocity += self.resistance_value
        self.velocity = min(self.velocity,self.max_speed)
        self.velocity = max(self.velocity,self.max_brake_speed)


class Border(pg.sprite.Sprite):
    def __init__(self, position, width, height, color):
        pg.sprite.Sprite.__init__(self)
        self.position = position

        self.image = pg.Surface([width, height])
        self.image.fill(color)
        pg.draw.rect(self.image, color, [0,0,width,height])
        self.rect = self.image.get_rect()
        self.rect.x = self.position.x
        self.rect.y = self.position.y

class FinishLine(pg.sprite.Sprite):
    def __init__(self, position):
        pg.sprite.Sprite.__init__(self)
        self.position = position

        self.image = pg.image.load('finishline.jpg')

        self.rect = self.image.get_rect()
        self.rect.x = self.position.x
        self.rect.y = self.position.y
